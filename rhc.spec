%define debug_package %{nil}

Name:    rhc
Version: 0.2.0
Release: 1%{?dist}
Epoch:   1
Summary: Message dispatch agent for cloud-connected systems
License: GPLv3
URL:     https://github.com/redhatinsights/yggdrasil

Source0: %{name}-%{version}.tar.gz
Source1: config.toml

ExclusiveArch: %{go_arches}

BuildRequires: git
BuildRequires: golang
BuildRequires: dbus-devel
BuildRequires: systemd-devel


%description
%{name} is pair of utilities that register systems with RHSM and establishes
a receiving queue for instructions to be sent to the system via a broker.

%prep
%autosetup


%build
export BUILDFLAGS='-buildmode pie'
make PREFIX=%{_prefix} \
     SYSCONFDIR=%{_sysconfdir} \
     LOCALSTATEDIR=%{_localstatedir} \
     SHORTNAME=%{name} \
     LONGNAME=%{name} \
     PKGNAME=%{name} \
     'BRANDNAME=Red Hat connector' \
     TOPICPREFIX=redhat/insights \
     VERSION=%{version} \
     DATAHOST=cert.cloud.redhat.com \
     'PROVIDER=Red Hat'


%install
export BUILDFLAGS='-buildmode pie'
make PREFIX=%{_prefix} \
     SYSCONFDIR=%{_sysconfdir} \
     LOCALSTATEDIR=%{_localstatedir} \
     DESTDIR=%{buildroot} \
     SHORTNAME=%{name} \
     LONGNAME=%{name} \
     PKGNAME=%{name} \
     'BRANDNAME=Red Hat connector' \
     TOPICPREFIX=redhat/insights \
     VERSION=%{version} \
     DATAHOST=cert.cloud.redhat.com \
     'PROVIDER=Red Hat' \
     install
%{__install} -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/%{name}/


%files
%doc README.md doc/tags.toml
%{_bindir}/%{name}
%{_sbindir}/%{name}d
%config(noreplace) %{_sysconfdir}/%{name}/config.toml
%{_unitdir}/%{name}d.service
%{_datadir}/bash-completion/completions/*
%{_mandir}/man1/*
%{_prefix}/share/pkgconfig/%{name}.pc
%{_libexecdir}/%{name}


%changelog
* Mon Jun 28 2021 Link Dupont <link@redhat.com> - 0.2.0-1
- New upstream release

* Fri Jun 25 2021 Link Dupont <link@redhat.com> - 0.1.99-5
- Mark config file as such

* Fri Jun 25 2021 Link Dupont <link@redhat.com> - 0.1.99-4
- New upstream snapshot

* Fri Jun 11 2021 Link Dupont <link@redhat.com> - 0.1.99-3
- Build executables as PIE programs

* Thu Jun 10 2021 Link Dupont <link@redhat.com> - 0.1.99-2
- Include missing disttag

* Tue May 25 2021 Link Dupont <link@redhat.com> - 0.1.99-1
- New upstream development release

* Wed Apr 28 2021 Link Dupont <link@redhat.com> - 0.1.4-2
- Rebuild for fixed binutils on aarch64 (Resolves: RHBZ#1954449)

* Fri Apr  9 2021 Link Dupont <link@redhat.com> - 0.1.4-1
- New upstream release

* Fri Feb 19 2021 Link Dupont <link@redhat.com> - 0.1.2-2
- Update default broker URI
- Set Epoch to 1

* Thu Feb 18 2021 Link Dupont <link@redhat.com> - 0.1.2-1
- New upstream release

* Wed Feb 17 2021 Link Dupont <link@redhat.com> - 0.1.1-1
- New upstream release

* Fri Feb 12 2021 Link Dupont <link@redhat.com> - 0.1-1
- Initial release
